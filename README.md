# GData Science and Machine Learning Working Meeting

https://gitlab.com/nukespud/data-science

## Day 1 
1)  Introduction to Data Science
1) [Git version control and command line](gitlabBasics.md)
1)  [Python Introduction and Numpy](https://colab.research.google.com/drive/1KuZd0q3MPYPY44WDzRpGKNcYTSgU2ius)
1) [Data Processing with Python](https://colab.research.google.com/drive/1M1nLS4nqIsPzU72Mrg37Df4jgmfhbqds)

### Lunch (On your own)
1) [Sampling](https://gitlab.com/CEADS/DrKerby/python/)
1) [Python Pandas GroupBy](https://gitlab.com/CEADS/DrKerby/python/)
1) [Simple Machine Learning](https://gitlab.com/CEADS/DrKerby/python/)
1) [Python Pandas,Functions, & Histograms](https://gitlab.com/CEADS/DrKerby/python/)
1) [Brainstorm Funding](https://gitlab.com/CEADS/DrKerby/python/)

## Day 2

1) [Introduction to Machine Learning](https://gitlab.com/CEADS/DrKerby/python/)
2) [Data Visualization with Bokeh](https://colab.research.google.com/drive/11Cg0w8oGzSmYOm4B63jI2W8p4xTySuWX)
3) [Predicting with Decision Trees](https://gitlab.com/CEADS/DrKerby/python/)
4) [Natural Language Processing with NLTK](https://colab.research.google.com/drive/18TN4sXYVLDDhdZ8ttdMRupUs62XjFuRO)

### Lunch (On your own)

1) [Case Study: Credit Card Defaults](https://gitlab.com/CEADS/DrKerby/python/)
1) [Clustering with k-Menas](https://gitlab.com/CEADS/DrKerby/python/)
1) [Brainstorm Funding](https://gitlab.com/CEADS/DrKerby/python/)