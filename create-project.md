# Create a project

Projects combine many features of GitLab together.


To create a project in GitLab:

1. In your dashboard, click the green **New project** button or use the plus
   icon in the navigation bar. This opens the **New project** page.
1. On the **New project** page, choose if you want to:
   - Create a [blank project](#blank-projects).
   - Create a project using with one of the available [project templates](#project-templates).
   - Import a project from a different repository,
     if enabled on your GitLab instance. Contact your GitLab admin if this
     is unavailable.

## Blank projects

To create a new blank project on the **New project** page:

1. On the **Blank project** tab, provide the following information:
    - The name of your project in the **Project name** field. You can't use
      special characters, but you can use spaces, hyphens, underscores or even
      emoji.
    - The **Project description (optional)** field enables you to enter a
      description for your project's dashboard, which will help others
      understand what your project is about. Though it's not required, it's a good
      idea to fill this in.
    - Changing the **Visibility Level** modifies the project's
      viewing and access rights for users.
    - Selecting the **Initialize repository with a README** option creates a
      README file so that the Git repository is initialized, has a default branch, and
      can be cloned.
1. Click **Create project**.


## Push to create a new project

> [Introduced](https://gitlab.com/gitlab-org/gitlab-ce/issues/26388) in GitLab 10.5.

When you create a new repo locally, instead of going to GitLab to manually
create a new project and then push the repo, you can directly push it to
GitLab to create the new project, all without leaving your terminal. If you have access to that
namespace, we will automatically create a new project under that GitLab namespace with its
visibility set to Private by default (you can later change it in the [project's settings](../public_access/public_access.md#how-to-change-project-visibility)).

This can be done by using either SSH or HTTPS:

```sh
## Git push using SSH
git push --set-upstream git@gitlab.example.com:namespace/nonexistent-project.git master

## Git push using HTTPS
git push --set-upstream https://gitlab.example.com/namespace/nonexistent-project.git master
```

Once the push finishes successfully, a remote message will indicate
the command to set the remote and the URL to the new project:

```text
remote:
remote: The private project namespace/nonexistent-project was created.
remote:
remote: To configure the remote, run:
remote:   git remote add origin https://gitlab.example.com/namespace/nonexistent-project.git
remote:
remote: To view the project, visit:
remote:   https://gitlab.example.com/namespace/nonexistent-project
remote:
```
